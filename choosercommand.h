#ifndef CHOOSERCOMMAND_H
#define CHOOSERCOMMAND_H

#include <interfaces/iaction.h>
#include <interfaces/iactionchooser.h>
#include <interfaces/iactionchooser.h>
#include <interfaces/ichoosercommand.h>


class ChooserCommand : public IChooserCommand
{
    IActionChooser* object;
    IAction *(IActionChooser:: *method)(IState *state);
  public:
    ChooserCommand(IActionChooser *obj = 0, IAction *(IActionChooser:: *meth)(IState *) = 0);
    ChooserCommand();
    void setObject(IActionChooser*obj);
    void setMethod(IAction *(IActionChooser:: *method)(IState *state));
    IAction * execute(IState *);
    ~ChooserCommand();
};

#endif // CHOOSERCOMMAND_H
