#ifndef UIFORM_H
#define UIFORM_H

#include <QGraphicsView>
#include <QWidget>

#include <interfaces/ienviroment.h>

namespace Ui {
class UIForm;
}

class UIForm : public QWidget
{
    Q_OBJECT

public:

    void setEnviroment(IEnviroment *wdg);
    explicit UIForm(QWidget *parent = 0);
    ~UIForm();

private:
    IEnviroment *enviroment;
    Ui::UIForm *ui;
public slots:
    void nextButtonPressed();
    void learnButtonPressed();
};

#endif // UIFORM_H
