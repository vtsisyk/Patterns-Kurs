#include "action.h"

double Action::getQ()
{
    return q;
}

void Action::setQ(double _q)
{
    q = _q;
}

double Action::getReward()
{
    return reward;
}

void Action::setReward(double _reward)
{
    reward = _reward;
}

void Action::setNext(IState * n)
{
    next = n;
}

IState *Action::getNext()
{
    return next;
}

Action::Action()
{
    q = 0;
    reward = 0;
    next = nullptr;
}

Action::Action(IState * n)
{
    q = 0;
    reward = 0;
    next = n;
}

