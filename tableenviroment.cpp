#include "tableenviroment.h"



void TableEnviroment::setInitial(IState *state)
{
    initial = state;
}

void TableEnviroment::setAgent(IAgent *robot)
{
    agent = robot;
    agent->attach(this);

}

void TableEnviroment::setDrawer(IDrawer *draw)
{
    drawer = draw;
    drawer->attach(this);
}

void TableEnviroment::setOptimization(IQLearning *opt)
{
    data = dynamic_cast<IQData *>(opt);

    if(data == nullptr){
        // error
    }

}

void TableEnviroment::runIteration()
{
    while(1){
        getNextAction();
        goNextState();
        if(agent->currentState() == data->getEndState())
            break;
    }
    setState(initial);
}

IDrawer *TableEnviroment::getDrawer()
{
    return drawer;
}

void TableEnviroment::getNextAction()
{
    if(agent->currentState() != data->getEndState()){
        IAction *act = data->move(&IActionChooser::greedyAction);
        drawer->updateLabels(agent->currentState());
        agent->notify(act);
    } else {
        setState(initial);
        getNextAction();
    }
}

void TableEnviroment::goNextState()
{
    if(agent->currentState() != data->getEndState() ){
        data->setStart(agent->nextState());
        drawer->changeState(agent->nextState());
        agent->notify(agent->nextState());
    }
}

void TableEnviroment::setState(IState *state)
{
    agent->notify(state);
    data->setStart(state);
    drawer->setState(state);
}

TableEnviroment::TableEnviroment(IAgent *robot, IDrawer *draw, IQLearning *learn)
{
    setAgent(robot);
    setDrawer(draw);
    setOptimization(learn);
}
