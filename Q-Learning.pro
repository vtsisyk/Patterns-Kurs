TEMPLATE = app
CONFIG += qt
QT +=  widgets

SOURCES += main.cpp \
    interfaces/istate.cpp \
    interfaces/iqdata.cpp \
    qltable.cpp \
    state.cpp \
    interfaces/iaction.cpp \
    action.cpp \
    interfaces/iactionchooser.cpp \
    chooser.cpp \
    interfaces/ichoosercommand.cpp \
    choosercommand.cpp \
    interfaces/iqlearning.cpp \
    tableenviroment.cpp \
    tableui/edge.cpp \
    tableui/graphwidget.cpp \
    tableui/node.cpp \
    interfaces/idrawer.cpp \
    tablerobot.cpp \
    interfaces/ienviroment.cpp \
    interfaces/iagent.cpp \
    uiform.cpp

HEADERS += \
    interfaces/istate.h \
    interfaces/iqdata.h \
    qltable.h \
    state.h \
    interfaces/iaction.h \
    action.h \
    interfaces/iactionchooser.h \
    chooser.h \
    interfaces/ichoosercommand.h \
    choosercommand.h \
    interfaces/iqlearning.h \
    tableenviroment.h \
    tableui/edge.h \
    tableui/graphwidget.h \
    tableui/node.h \
    interfaces/idrawer.h \
    tablerobot.h \
    interfaces/ienviroment.h \
    interfaces/iagent.h \
    uiform.h

FORMS += \
    uiform.ui
