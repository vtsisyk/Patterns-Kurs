#ifndef STATE_H
#define STATE_H

#include "action.h"
#include <interfaces/istate.h>
#include <vector>

class State : public IState
{
private:
    std::vector<IAction *> actions;
public:
    int getActionsSize();
    IAction *getActionAt(int i);
    void addAction(IAction *action);
    void rmAllActions();
    void printState();
    double getMaxQ();
    State();

};

#endif // STATE_H
