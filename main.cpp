#include <QtCore/QCoreApplication>
#include <QDebug>
#include <tableui/graphwidget.h>



#include "state.h"
#include "qltable.h"
#include "qltable.cpp"
#include "tablerobot.h"
#include "tableenviroment.h"
#include "uiform.h"

#include <iostream>


#include <QApplication>
#include <QMainWindow>
#include <QTime>
#include <iostream>
#include <string>
#include <string.h>


using namespace std;


int main(int argc, char *argv[]){

    QApplication app(argc, argv);
    QLTable<Action, State> t(3,4);
    TableRobot robot("walker");

   // t.setObstacle(1,1);
    for(int i = 0; i < 3; i++){
        for(int j = 0; j < 4; j++){
            t.addAction(i, j, i, j + 1);
            t.addAction(i, j, i + 1, j);
            t.addAction(i, j, i - 1, j);
            t.addAction(i, j, i, j -1);
        }
    }
    t.setEnd(0,3,1);
    t.setEnd(1,3,-1);
    t.setEnd(1,2,-1);

    GraphWidget *widget = new GraphWidget(nullptr);

    //    IState *st = t.getAt(0,0);
    //    for(int i = 0; i < 100; i++){
    //        st = t.runEpisode(st);
    //        st = t.getAt(0,0);

    //    }
    widget->setData(&t);
    TableEnviroment table(&robot, widget, &t);
    table.setInitial(t.getAt(2,2));
    robot.setState(t.getAt(2,2));
    //  robot.setState(t.getAt(0,0));
    UIForm form;
    form.setEnviroment(&table);
    //  mainWindow.setCentralWidget(widget);
    // mainWindow.show();
    form.show();



    return app.exec();
}
