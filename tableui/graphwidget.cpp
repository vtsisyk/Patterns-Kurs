/****************************************************************************
**
** Copyright (C) 2016 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** BSD License Usage
** Alternatively, you may use this file under the terms of the BSD license
** as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "graphwidget.h"
#include "edge.h"
#include "node.h"
#include <math.h>
#include <QKeyEvent>
#include <qltable.h>
#include <qltable.cpp>

GraphWidget::GraphWidget(QWidget *parent)
    : QGraphicsView(parent)
{
    sce = new QGraphicsScene(this);
    sce->setItemIndexMethod(QGraphicsScene::NoIndex);
    sce->setSceneRect(-400, -400, 800, 800);
    setScene(sce);
    setCacheMode(CacheNone);
    setViewportUpdateMode(BoundingRectViewportUpdate);
    setRenderHint(QPainter::Antialiasing);
    setTransformationAnchor(AnchorUnderMouse);
    scale(qreal(1), qreal(1));
    setMinimumSize(800, 800);
    setWindowTitle(tr("Q-Learning"));

}

void GraphWidget::setData(IQData *tabledata)
{
    IQData *table = tabledata;

    QLTable<Action, State> *qtable = dynamic_cast<QLTable<Action, State> *>(table);
    std::vector<std::vector<IState *>> map = qtable->getTable();
    toshow.resize(qtable->getWidth());
    for(auto &it : toshow) {
        it.resize(qtable->getHeight());
    }

    for(int i = 0; i < qtable->getWidth(); i++){
        for(int j = 0; j < qtable->getHeight(); j++){
            if(map[i][j]->getActionsSize() !=0){
                toshow[i][j] = new Node(this, map[i][j]);
                sce->addItem(toshow[i][j]);
                toshow[i][j]->setPos((i ) * 100, (j ) *100);
            }
        }
    }

    endst = new Node(this, qtable->getEndState());
    endst->setPos((qtable->getWidth()) * 100, (qtable->getHeight() ) * 100);
    sce->addItem(endst);
    endst->changeColor(QColor(0,0,0));

    for(int i = 0; i < qtable->getWidth(); i++){
        for(int j = 0; j < qtable->getHeight(); j++){
            IState *st = map[i][j];
            for(int k = 0; k < st->getActionsSize(); k++){
                IAction *act = st->getActionAt(k);
                IState *next = act->getNext();

                int d,s;
                bool end = false;
                for(d = 0 ; d < qtable->getWidth(); d++){
                    for(s =0; s < qtable->getHeight(); s++){
                        if(map[d][s] == next){
                            end = true;
                            break;
                        }
                    }
                    if(end)
                        break;
                }
                if(end == false) {
                    Edge *edg = new Edge(toshow[i][j], endst, act);
                    sce->addItem(edg);
                } else {
                    Edge *edg = new Edge(toshow[i][j], toshow[d][s], act);
                    sce->addItem(edg);
                }
                toshow[i][j]->update();
            }
        }
    }
}

void GraphWidget::updateLabels(IState *st)
{
    Node *nd = findNode(st);
    for(Edge *edge: nd->edges()){
        edge->reset();
    }
}

void GraphWidget::attach(IEnviroment *env)
{
    enviroment = env;
}

void GraphWidget::changeState(IState *st)
{
    currentNode->reset();
    currentNode->update();
    currentNode = currentNode->goToState(st);
    currentNode->changeColor(QColor(0,0,255));
    currentNode->update();
}

void GraphWidget::setState(IState *st)
{
    Node *nd = findNode(st);
    currentNode = nd;
    currentNode->changeColor(QColor(0,0,255));
    endst->changeColor(QColor(0,0,0));
    endst->update();
    return;

}

void GraphWidget::mouseDoubleClickEvent( QMouseEvent * e )
{
    QList<QGraphicsItem *> lst;
    if ( e->button() == Qt::LeftButton ){
        lst = items(e->pos());
        for(QGraphicsItem *it: lst){
            if(dynamic_cast<Node *>(it) !=nullptr){
                changeState(dynamic_cast<Node *>(it)->getState());
                enviroment->setState(currentNode->getState());
                break;
            }
        }
    }
    QGraphicsView::mouseDoubleClickEvent(e);
}

Node * GraphWidget::findNode(IState *st)
{
    for(unsigned int i = 0; i < toshow.size(); i++){
        for(unsigned int j = 0; j < toshow[i].size(); j++){
            if(toshow[i][j] != nullptr && toshow[i][j]->isthisState(st)){
                return toshow[i][j];
            }
        }
    }
    return nullptr;
}



#if QT_CONFIG(wheelevent)

void GraphWidget::wheelEvent(QWheelEvent *event)
{
    scaleView(pow((double)2, event->delta() / 240.0));
}

#endif


void GraphWidget::scaleView(qreal scaleFactor)
{
    qreal factor = transform().scale(scaleFactor, scaleFactor).mapRect(QRectF(0, 0, 1, 1)).width();
    if (factor < 0.07 || factor > 100)
        return;

    scale(scaleFactor, scaleFactor);
}


void GraphWidget::shuffle()
{
    foreach (QGraphicsItem *item, scene()->items()) {
        if (qgraphicsitem_cast<Node *>(item))
            item->setPos(-150 + qrand() % 300, -150 + qrand() % 300);
    }
}

void GraphWidget::zoomIn()
{
    scaleView(qreal(1.2));
}

void GraphWidget::zoomOut()
{
    scaleView(1 / qreal(1.2));
}
