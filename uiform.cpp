#include "uiform.h"
#include "ui_uiform.h"

void UIForm::setEnviroment(IEnviroment *wdg)
{


    QGraphicsView *v = dynamic_cast<QGraphicsView *>(wdg->getDrawer());
    if(v != nullptr)
        ui->verticalLayout->addWidget(v);
    enviroment = wdg;
}

UIForm::UIForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::UIForm)
{
    ui->setupUi(this);
    connect(ui->nextButton, SIGNAL(clicked(bool)), this, SLOT(nextButtonPressed()));
    connect(ui->learnButton, SIGNAL(clicked(bool)), this, SLOT(learnButtonPressed()));
}

UIForm::~UIForm()
{
    delete ui;
}

void UIForm::nextButtonPressed()
{
    enviroment->getNextAction();
    enviroment->goNextState();
}

void UIForm::learnButtonPressed()
{
    int iterations = ui->iterationsSpinBox->text().toInt();

    for(int i = 0; i < iterations; i++){
        enviroment->runIteration();
    }
}
