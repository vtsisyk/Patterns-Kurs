#ifndef IQDATA_H
#define IQDATA_H

#include "iactionchooser.h"
#include "iqlearning.h"
#include "istate.h"

#include <vector>

class IQData : public IQLearning
{
public:
    virtual std::vector<std::vector<IState *>> getTable() = 0;
    virtual IState *getEndState() = 0;
    virtual IAction *move() = 0;
    virtual IAction *move(IAction *(IActionChooser:: *method)(IState *state)) = 0;
    virtual void setStart(IState *init) = 0;
    IQData();
};

#endif // IQDATA_H
