#ifndef IDRAWER_H
#define IDRAWER_H

#include "ienviroment.h"
#include "istate.h"
class IEnviroment;


class IDrawer
{
public:
    virtual void changeState(IState *st) = 0;
    virtual void setState(IState *st) = 0;
    virtual void attach(IEnviroment *env) =0;
    virtual void updateLabels(IState *st) = 0;
    IDrawer();
};

#endif // IDRAWER_H
