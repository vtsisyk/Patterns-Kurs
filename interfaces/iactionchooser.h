#ifndef IACTIONCHOOSER_H
#define IACTIONCHOOSER_H

#include "iaction.h"



class IActionChooser
{
public:
    virtual IAction *randomAction(IState *state) = 0;
    virtual IAction *greedyAction(IState *state) = 0;
    IActionChooser();
    virtual ~IActionChooser() = 0;
};

#endif // IACTIONCHOOSER_H
