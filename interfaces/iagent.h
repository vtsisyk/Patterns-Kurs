#ifndef IAGENT_H
#define IAGENT_H

#include "iaction.h"
#include "ienviroment.h"

class IAgent
{
public:
    IAgent();
    virtual void attach(IEnviroment *env) = 0;
    virtual void notify(IAction *act) = 0;
    virtual void notify(IState *st) = 0;
    virtual void getNextAction() = 0;
    virtual void setState(IState *state) = 0;
    virtual IState *nextState() = 0 ;
    virtual IState *currentState() = 0;
};

#endif // IAGENT_H
