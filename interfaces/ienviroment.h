#ifndef IENVIROMENT_H
#define IENVIROMENT_H

#include "iaction.h"
#include "idrawer.h"
class IDrawer;

class IEnviroment
{
public:

    virtual void getNextAction() = 0;
    virtual void goNextState() = 0;
    virtual void setState(IState *state) = 0;
    virtual IDrawer *getDrawer() = 0;
    virtual void runIteration() = 0;
    IEnviroment();
};

#endif // IENVIROMENT_H
