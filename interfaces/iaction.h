#ifndef IACTION_H
#define IACTION_H
class IState;

#include "istate.h"



class IAction
{
public:
    virtual double getQ() = 0;
    virtual IState *getNext() = 0;
    virtual void setNext(IState *) = 0;
    virtual void setQ(double _reward) = 0;

    virtual double getReward() = 0;
    virtual void setReward(double _reward) = 0;

    IAction();

};

#endif // IACTION_H
