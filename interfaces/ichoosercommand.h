#ifndef ICHOOSERCOMMAND_H
#define ICHOOSERCOMMAND_H

#include "iaction.h"
#include "iactionchooser.h"

class IChooserCommand
{
public:
    virtual void setObject(IActionChooser*obj) = 0;
    virtual void setMethod(IAction *(IActionChooser:: *method)(IState *state)) = 0;
    virtual IAction * execute(IState *) = 0;
    IChooserCommand();
    virtual ~IChooserCommand() = 0;
};

#endif // ICHOOSERCOMMAND_H
