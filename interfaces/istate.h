#ifndef ISTATE_H
#define ISTATE_H
class IAction;
#include "iaction.h"

class IState
{
public:
    IState();
    virtual int getActionsSize() = 0;
    virtual IAction *getActionAt(int i) = 0;

    virtual void addAction(IAction *action) = 0;
    virtual void printState() = 0;
    virtual void rmAllActions() = 0;
    virtual double getMaxQ()= 0;

};

#endif // ISTATE_H
