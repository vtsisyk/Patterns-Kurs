#ifndef TABLEROBOT_H
#define TABLEROBOT_H

#include <interfaces/iqlearning.h>
#include <interfaces/iagent.h>
#include <interfaces/istate.h>
#include <interfaces/idrawer.h>
#include <interfaces/iqdata.h>

#include <string>

class TableRobot : public IAgent
{
private:
    std::string name;
    IState *current = nullptr;
    IAction *action = nullptr;
    IEnviroment *enviroment;
public:

    void attach(IEnviroment *env);
    void notify(IAction *act);
    void notify(IState *st);
    TableRobot();
    void getNextAction();
    void goNextState();

    IState *currentState();
    IState *nextState();
    void setState(IState *state);
    TableRobot(std::string nm);
};

#endif // TABLEROBOT_H
