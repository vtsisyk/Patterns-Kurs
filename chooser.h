#ifndef CHOOSER_H
#define CHOOSER_H
#include <random>
#include <interfaces/iaction.h>
#include <interfaces/iactionchooser.h>



class Chooser : public IActionChooser
{
private:

public:
    Chooser();
    IAction *randomAction(IState *state);
    IAction *greedyAction(IState *state);
    /*TODO add epsilon-greedy*/
    ~Chooser();
};

#endif // CHOOSER_H
