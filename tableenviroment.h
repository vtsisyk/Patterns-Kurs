#ifndef TABLEENVIROMENT_H
#define TABLEENVIROMENT_H

#include <interfaces/idrawer.h>
#include <interfaces/ienviroment.h>
#include <interfaces/iqlearning.h>
#include <interfaces/iagent.h>
#include <interfaces/iqdata.h>


class TableEnviroment : public IEnviroment
{
    IAgent *agent;
    IDrawer *drawer;
    IQData *data;

    IState *initial;

public:
    void setInitial(IState *state);
    void setAgent(IAgent *robot);
    void setDrawer(IDrawer *draw);
    void setOptimization(IQLearning *opt);

    void runIteration();

    IDrawer *getDrawer();


    void getNextAction();
    void goNextState();
    void setState(IState *state);
    /* domo arigato */
    TableEnviroment(IAgent *robot, IDrawer *draw, IQLearning *learn);
};

#endif // TABLEENVIROMENT_H
