#ifndef ACTION_H
#define ACTION_H

#include "state.h"

#include <interfaces/iaction.h>



class Action : public IAction
{
private:
    IState *next;
    //QValue<double> reward;
    double reward;
    double q;
public:
    double getQ();
    void setQ(double _q);

    double getReward();
    void setReward(double _reward);
    void setNext(IState *n);
    IState *getNext();
    Action();
    Action(IState *);
//    ~Action();
};

#endif // ACTION_H
