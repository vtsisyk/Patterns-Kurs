#ifndef QLTABLE_H
#define QLTABLE_H

#include "chooser.h"
#include "choosercommand.h"
#include "state.h"

#include <interfaces/iqdata.h>

#include <vector>


template <class A, class S>
class QLTable : public IQData
{
private:
    int width, height;
    std::vector<IState *> obstacles;

    double alpha = 0.7;
    double gamma = 0.65;

    IState *endState; /* это единственное настоящее конечное состояние*/
    IActionChooser *chooser;
    IChooserCommand *cmd;
    IState *current;

    void resizeVec(std::vector<std::vector<IState *>> &vec, const int rows , const int columns );

    std::vector<std::vector<IState *>> map;

public:

    QLTable();
    void setGamma(double g);
    void setAlpha(double a);

    int getWidth();
    int getHeight();
    void addAction(int from_x, int from_y, int to_x, int to_y);
    void setObstacle(int x, int y);
    void setEnd(int x, int y, double reward);
    bool isObstacle(IState *state);
    IState *getAt(int x, int y);

    IState *getEndState();
    void debugPrint();
    QLTable(int width, int height);
    IState *runEpisode(IState *init);

    IAction *move();
    IAction *move(IAction *(IActionChooser:: *method)(IState *state));
    void setStart(IState *init);

    std::vector<std::vector<IState *> > getTable();
    ~QLTable();
};



#endif // QLTABLE_H
