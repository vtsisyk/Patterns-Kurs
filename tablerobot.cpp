#include "tablerobot.h"

void TableRobot::attach(IEnviroment *env)
{
    enviroment = env;
}

void TableRobot::notify(IAction *act)
{
    action = act;
}

void TableRobot::notify(IState *st)
{
    current = st;
}

TableRobot::TableRobot()
{
    action = nullptr;
}

void TableRobot::getNextAction()
{
    enviroment->getNextAction();
}

void TableRobot::goNextState()
{
    enviroment->goNextState();
}

IState *TableRobot::currentState()
{
    return current;
}

IState *TableRobot::nextState()
{
    if(action == nullptr)
        return nullptr;

    return action->getNext();
}

void TableRobot::setState(IState *state)
{
    enviroment->setState(state);
}

TableRobot::TableRobot(std::__cxx11::string nm)
{
    name = nm;
    action = nullptr;
}
