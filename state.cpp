#include "state.h"
#include <iostream>
#include <QDebug>
int State::getActionsSize()
{
    return actions.size();
}

IAction* State::getActionAt(int i)
{
    if((int)actions.size() > i)
        return actions[i];
    return nullptr;
}

void State::addAction(IAction *action)
{
    /* TODO: проверить, есть ли такое же действие, не nullptr ли */
    actions.push_back(action);
}


void State::rmAllActions()
{
    actions.clear();
}

void State::printState()
{
    std::cout << "["<< this << "]\n";
    std::cout << "-- Q --\n";
    for(IAction *action: actions){
        qDebug() << action;
        IState *st=  action->getNext();
        std::cout << " -> " << "[";
        if(st == nullptr)
            std::cout << "nullptr" << "](";
        else
            std::cout << st << "](";
        std::cout<< action->getQ() << ")\n";
    }
}

double State::getMaxQ()
{
    if(actions.size() > 0){
        double max = actions[0]->getQ();
        for(IAction *action : actions)
            if(action->getQ() > max)
                max = action->getQ();

        return max;
    }
    return 0;
}

State::State()
{

}

