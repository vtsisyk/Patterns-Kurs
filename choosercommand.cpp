#include "choosercommand.h"

ChooserCommand::ChooserCommand(IActionChooser *obj, IAction *(IActionChooser::*meth)(IState *))
{
    object = obj;
    method = meth;
}

void ChooserCommand::setObject(IActionChooser *obj)
{
    object = obj;
}

void ChooserCommand::setMethod(IAction *(IActionChooser::*meth)(IState *))
{
    method = meth;
}

IAction *ChooserCommand::execute(IState * state)
{
    if(object !=nullptr && method !=nullptr)
        return (object->*method)(state);
    return nullptr;
}

ChooserCommand::~ChooserCommand()
{

}

ChooserCommand::ChooserCommand()
{
    object = nullptr;
    method = nullptr;
}
