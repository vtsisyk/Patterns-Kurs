#include "qltable.h"
#include <iostream>
#include <random>
template <class A, class S>
void  QLTable<A,S>::resizeVec(std::vector<std::vector<IState *> > &vec, const int rows, const int columns)
{
    vec.resize(rows);
    for(auto &it : vec) {
        it.resize(columns);
    }
    for(int i = 0; i < width; i++){
        for(int j = 0; j < height; j++)
            map[i][j] = new State();
    }
}
template <class A, class S>
QLTable<A,S>::QLTable()
{

}

template<class A, class S>
void QLTable<A,S>::setGamma(double g)
{
    gamma = g;
}

template<class A, class S>
void QLTable<A,S>::setAlpha(double a)
{
    alpha = a;
}

template<class A, class S>
int QLTable<A,S>::getWidth()
{
    return width;
}

template<class A, class S>
int QLTable<A,S>::getHeight()
{
    return height;
}

template <class A, class S>
void  QLTable<A,S>::addAction(int from_x, int from_y, int to_x, int to_y)
{
    if(from_x < 0 || to_x > width - 1 || from_y < 0 || to_y > height - 1 ||to_x < 0 || to_y < 0)
        return;

    if(isObstacle(map[to_x][to_y]) ||isObstacle(map[from_x][from_y]))
        return;
    IState *st = map[from_x][from_y];
    st->addAction(new A(map[to_x][to_y]));

}
template <class A, class S>
void QLTable<A,S>::setObstacle(int x, int y)
{
    map[x][y]->rmAllActions();
    obstacles.push_back(map[x][y]);
}
template <class A, class S>
QLTable<A,S>::QLTable(int w, int h)
{
    width = w;
    height = h;
    resizeVec(map, width,height);
    chooser = new Chooser();
    cmd = new ChooserCommand(chooser, &IActionChooser::randomAction);
    endState = new State();
}
template <class A, class S>
IState * QLTable<A,S>::runEpisode(IState *init)
{

    setStart(init);

    while(1){
        move();
        if(current == endState){
            return nullptr;
        }
    }

}

template<class A, class S>
IAction *QLTable<A,S>::move()
{
    return move(&IActionChooser::randomAction);
}

template<class A, class S>
IAction *QLTable<A,S>::move(IAction *(IActionChooser::*method)(IState *))
{
    cmd->setMethod(method);
    IAction *action = cmd->execute(current);
    IState *nextState = action->getNext();
    double target = 0;
    if(nextState == endState){
        target = action->getReward();
    } else {
        target = action->getReward() + gamma * nextState->getMaxQ();
    }
    action->setQ((1-alpha)*action->getQ() + alpha * target);

    current = nextState;
    return action;
}

template<class A, class S>
void QLTable<A,S>::setStart(IState *init)
{
    current = init;
}

template<class A, class S>
std::vector<std::vector<IState *> > QLTable<A,S>::getTable()
{
    return map;
}

template<class A, class S>
QLTable<A,S>::~QLTable()
{
    delete chooser;
    delete cmd;
    delete endState;

    for(int i = 0; i < width; i++){
        for(int j = 0; j < height; j++)
            delete map[i][j];
    }
}

template <class A, class S>
void  QLTable<A,S>::setEnd(int x, int y, double reward)
{
    IState *state = map[x][y];
    state->rmAllActions();

    IAction *act = new A(endState);
    act->setReward(reward);
    state->addAction(act);
}
template <class A, class S>
bool  QLTable<A,S>::isObstacle(IState *state)
{
    for(IState *st: obstacles)
        if(st == state)
            return true;
    return false;
}
template <class A, class S>
IState * QLTable<A,S>::getAt(int x, int y)
{
    return map[x][y];
}

template<class A, class S>
IState *QLTable<A,S>::getEndState()
{
    return endState;
}

template<class A, class S>
void QLTable<A,S>::debugPrint()
{
    for(int i = 0; i < width; i++){
        for(int j = 0; j < height; j++){
            std::cout << "----------------\n";
            std::cout << "[" << i << "]" << "[" << j << "]\n";
            map[i][j]->printState();

        }
    }
    std::cout << "Endstate: [" <<  endState << "]\n";

}

