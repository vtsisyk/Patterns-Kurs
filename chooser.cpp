#include "chooser.h"

Chooser::Chooser()
{

}

IAction *Chooser::randomAction(IState *state)
{
    if(state->getActionsSize() == 0)
        return nullptr;
    std::random_device device;
    std::mt19937 generator(device());
    std::uniform_int_distribution<int> distribution(0,state->getActionsSize() - 1);
    return state->getActionAt(distribution(generator));
}

IAction *Chooser::greedyAction(IState *state)
{
    if(state->getActionsSize() > 0){
        IAction *act = state->getActionAt(0);
        for(int i = 0; i < state->getActionsSize(); i++){
            if(state->getActionAt(i)->getQ() > act->getQ()){
                act= state->getActionAt(i);
            }else if(state->getActionAt(i)->getQ() == act->getQ()){
                /* well, it's not best solution, but it works */
                // TODO fix this
                std::random_device device;
                std::mt19937 generator(device());
                std::bernoulli_distribution coin_flip(0.5);
                bool outcome = coin_flip(generator);
                if(outcome)
                    act= state->getActionAt(i);;
            }
        }
        return act;
    }
    return nullptr;
}

Chooser::~Chooser()
{

}
